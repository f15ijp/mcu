'By dan-tron
'
'From http://www.picbasic.co.uk/forum/showthread.php?t=6979
'My main problem turned out to be that my compiler was too old and the 
'EEPROM READ  comand was not working properly. Here's my WORKING code 
'that was tested successfully on a PIC 16F628A with a 20MHz resonator 
'compiled using PBP v2.47. It reads data from the Parallax RFID Reader 
'Module, compares it against known values stored in EEPROM and allows 
'or denies access accordingly. Enjoy!

CMCON = 7   
DEFINE OSC 20	'Set oscillator in MHz

' -----[ Variables ]-------------------------------------------------------

buf	VAR	Byte(10)	' RFID bytes buffer
tagNum	VAR	Byte	' from EEPROM table
idx	VAR	Byte	' tag byte index
char	VAR	Byte	' character from table

' -----[ EEPROM Data ]-----------------------------------------------------

Tag1	DATA	"100050A4B7"
Tag2	DATA	"1000508E0A"
Tag3	DATA	"10005091DC"
Tag4	DATA	"100050203A"
Tag5	DATA	"100050DA36"

' -----[ Initialization ]--------------------------------------------------

HIGH portb.3	' turn off RFID reader
LOW portb.6	' lock the door!
Low portb.4	'Turn off LED

' -----[ Program Code ]----------------------------------------------------

Main:

LOW portb.3				' activate the reader
SERIN2 portb.2, 396, [WAIT($0A), STR buf\10]	' wait for hdr + ID
HIGH portb.3				' deactivate reader

Check_List:
  FOR tagNum = 1 to 5			' scan through known tags
    FOR idx = 0 TO 9				' scan bytes in tag
    READ (((tagNum-1) * 10) + idx), char		' get tag data from table
    IF (char <> buf(idx)) THEN Bad_Char		' compare tag to table
    NEXT
    GOTO Tag_Found				' all bytes match!
Bad_Char:					' try next tag
  NEXT

Bad_Tag:
  tagNum = 0
  FREQOUT portb.5, 1000 */ $100, 115 */ $100	' groan
  PAUSE 1000
  GOTO Main

Tag_Found:
  HIGH portb.6				' remove latch
  High portb.4				' Light LED
  FREQOUT portb.5, 2000 */ $100, 880 */$100	' beep
  LOW portb.6				' restore latch
  Low portb.4				' LED OFF
  GOTO Main
