;**********************************************************************
;   Enkelt test av CU40026 VFD display.
;   Testat med en "ISE Electronics CU40026SCPB-S 3.0"
;   F�r PIC16F887
;
;**********************************************************************
;                                                                     *
;    Filename:	    CU40026_16F887.asm
;    Date:          2012-06-14
;    File Version:  1.0
;
;    Author:        Jan-Erik S�derholm
;    Company:       Jan-Erik S�derholm Consulting AB
;
;**********************************************************************
;
;    Files required:  P16F887.INC
;
;**********************************************************************
;
;   Anv�nder intosc p� 4 Mhz.
;
;   /CS     : RD0
;   /RD     : RD1
;   A0      : RD2
;   /WR     : RD3
;   D0 - D7 : RC0 - RC7
;
;**********************************************************************
;
;
	list      p=16f887             ; list directive to define processor
	#include <p16f887.inc>         ; processor specific variable definitions
;	
	__CONFIG _CONFIG1, _DEBUG_OFF & _LVP_OFF & _FCMEN_OFF & _IESO_OFF & _BOR_OFF & _CPD_OFF & _MCLRE_ON & _PWRTE_ON & _WDT_OFF & _INTRC_OSC_NOCLKOUT
	__CONFIG _CONFIG2, _WRT_OFF & _BOR21V
;
    errorlevel -302 
;
    #define     VFD_CS      PORTD, 0
    #define     VFD_RD      PORTD, 1
    #define     VFD_A0      PORTD, 2
    #define     VFD_WR      PORTD, 3
    #define     VFD_PORT    PORTC
;   
;
;**********************************************************************
RESET_VECTOR    CODE    0x000       ; processor reset vector
	    goto    start               ; go to beginning of program
;
;
INT_VECTOR      CODE    0x004       ; interrupt vector location
        retfie                      ; Ingen interrupt kod...
;        
;
;**********************************************************************
; Macro f�r att skicka en textstr�ng till VFD.
;
vfd_text    macro   text_label
        banksel eeadr               ; byt till r�tt bank...
        movlw   high text_label     ; bit 8-15 av adressen till texten
        movwf   eeadrh
        movlw   low text_label      ; bit 0-7 av adressen till texten
        movwf   eeadr

; Nu �r de tv� adressregistren laddade med r�tt adress.
; Anropa subrutinen f�r att h�mta texten och skriva ut den.
;
        pagesel vfd_send_text
        call    vfd_send_text
        banksel porta               ; L�mna rutinen i bank 0.
;
        endm

;
;**********************************************************************
; Huvudkoden.
; H�r hamnar vi vid reset eller power-on (fr�n RESET_VECTOR)
;
;
prog1    CODE 
start
;
; F�rst normal uppstart ej specifik f�r VFD testen
;
        banksel ansel               ; St�ng av alla analoga funktioner
        clrf    ansel
        clrf    anselh
        banksel trisa               ; Alla pinnar utg�ngar...
        clrf    trisa
        clrf    trisb
        clrf    trisc
        clrf    trisd
;        
        banksel intcon
        clrf    intcon
        banksel porta               ; se till att vi �r p� bank 0.
;
; S� d� var grundinst�llningen av processorn klar.
; Nu g�r vi vidare med VFD hanteringen...
;
; S�tt kontrollsignaler i utg�ngsl�ge.

        bcf     VFD_CS
        bsf     VFD_RD
        bsf     VFD_WR
        bcf     VFD_A0

; L�t VFD modulen starta upp ordentligt...
        call    delay_1s

; Reset av VFD...
        movlw   h'50'
        call    write_vfd_cmd
        call    delay_1s
        
; St�ll om till svensk teckentabell. Se datablad sidan 4.
        movlw   h'1E'
        call    write_vfd_data

; Skriv ut lite definierade texter med hj�lp av ett macro.
; Se slutet av koden f�r defintionerna av texterna.

        vfd_text    vfd_text1
        vfd_text    vfd_text2
        vfd_text    vfd_text3
        vfd_text    vfd_text4
        vfd_text    vfd_text5

loop
        goto loop                       ; G�r ingenting....
;

;**********************************************************************
; Lite rutiner f�r hantering av VFD.
;
; N�gra temp variabler som anv�nds av VFD rutinerna.
; L�gger dom i "shared memory" s� slipper vi banksel...
;
VFD_VARS        UDATA_SHR
VFD_TMP1        RES 1
VFD_TMP2        RES 1
;
; Slut p� variabler, h�r kommer koden...
;
VFDSEG        CODE
;

vfd_send_text
;**********************************************************
; S�nd en fast text till VFD.
; N�r rutinen anropas ska EEADR och EEADRH vara initierade
; med adressen i flash till text-bufferten. T.ex genom det
; vfd_text macro som finns i detta program.
; VFD-pos 1-40 f�r f�rsta raden och 41-80 f�r rad 2.
;
; L�s f�rsta positionen i texten ("VFD-pos")
;
        call    read_flash
        movwf   vfd_tmp1        ; Lagra pos p� VFD
        decf    vfd_tmp1, w     ; Mina med ett (1-80 => 0-79)
        call    write_vfd_cmd   ; Skicka pos-kommando
;
;
send_text_loop
;
; L�s n�sta position ("VFD-text") och skicka.
; Avsluta om datat/tecknet = h'00'
;
        call    inc_eeadr       ; N�sta pos i flash
        call    read_flash      ; H�mta tecken fr�n flash
        btfsc   status, z       ; Var det h'00' (slut p� text) ?
        goto    send_text_end   ; Ja, avsluta.
                                ; Nej, forts�tt...      
        call    write_vfd_data  ; Skicka tecken med data i W-reg.
        goto    send_text_loop  ; Om igen...
;
send_text_end

; Slut p� textstr�ng...
;
        return
;

read_flash
;
; L�s en byte fr�n flash
; EEADR och EEADRH ska redan vara initierade.
;
        banksel eecon1
        bsf     eecon1, eepgd   ; L�s fr�n flash...
        bsf     eecon1, rd      ; S�tt l�s-flaggan...
        nop                     ; V�nta p� att l�sning
        nop                     ; ska g� klart... 
        banksel eedat
        movf    eedat, w        ; Nu har vi v�rdet i W-reg.
        return
;
; R�kna fram eeadr/eeadrh till n�sta position
;
inc_eeadr
        banksel eeadr
        incf    eeadrh, f       ; �ka f�rst eeadrh
        incfsz  eeadr, f        ; �ka eeadr, = 0 ?
        decf    eeadrh, f       ; eeadr <> 0, �terst�ll eeadrh...
        return
;

write_vfd_data
        banksel porta
        bcf     VFD_A0
        movwf   VFD_PORT
        call toggle_WR
        return

write_vfd_cmd
        banksel porta
        bsf     VFD_A0
        movwf   VFD_PORT
        call toggle_WR
        return

toggle_WR

        call    delay_100us
        bcf     VFD_WR
        call    delay_100us
        bsf     VFD_WR
        call    delay_100us
        return


;        
;**********************************************************************
; Diverse delay rutiner.
; Alla delay rutiner generade med kodgeneratorn h�r :
; http://www.piclist.com/techref/piclist/codegen/delay.htm
; OBS generatade f�r 4 Mhz processorhastighet. 
;
DLY_VAR      UDATA_SHR
d1           RES 1
d2           RES 1
d3           RES 1
;
;
DLY_CODE        CODE
;
delay_1s
	movlw	0x08
	movwf	d1
	movlw	0x2F
	movwf	d2
	movlw	0x03
	movwf	d3
Delay_1s_0
	decfsz	d1, f
	goto	$+2
	decfsz	d2, f
	goto	$+2
	decfsz	d3, f
	goto	Delay_1s_0
	goto	$+1
	nop
    return
;
delay_5ms
       	movlw	0xE7
	    movwf	d1
    	movlw	0x04
    	movwf	d2
Delay_5ms_0
    	decfsz	d1, f
    	goto	$+2
    	decfsz	d2, f
    	goto	Delay_5ms_0
    	goto	$+1
        return
;
delay_100us
    	movlw	0x21
	    movwf	d1
Delay_100us_0
    	decfsz	d1, f
    	goto	Delay_100us_0
        return
; 
;**********************************************************************
VFD_texts   CODE
; Fasta texter f�r VFDn.
; F�rsta v�rdet �r position, 1-40 f�r f�rsta raden och 41-80 f�r andra.
; Resten �r text, avslutas med ett "null" (h'00').

vfd_text1   data    d'1', 'D','E','M','O', h'00'
vfd_text2   data    d'6', 'V','F','D',' ','C','U','4','0','0','2','6', h'00'
vfd_text3   data    d'18','P','I','C','1','6','F','8','8','7', h'00'
vfd_text4   data    d'28', '(',h'7D',h'7B',h'7C',h'5D',h'5B',h'5C',')', h'00'
vfd_text5   data    d'41', '(','S','o','d','j','a','n',')', h'00'
;
;**********************************************************************
    end
